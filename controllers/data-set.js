const res = require('express/lib/response');
const archiveUtils = require('../services/readArchive');
const dataSetService = require('../services/dataSet')
const constants = require('./../services/constants.js')
/**
 * recuper la liste des carte par maille et echelle.
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.getDataSetList = (req, res, next) => {
    const tempDirectory = "./data/tempFiles/";
    const mapBaseFolder = "./data/data_sets/";
    const jsonTarget = "data-set-info.json";

    //récupérer les paramètres de filtrage 
    let echelle = req.query.echelle;
    let maille = req.query.maille;

    if (echelle && maille) {

        //récupérer la liste des jeux de donnée depuis les archive
        let dataSetList = archiveUtils.readZipArchive(mapBaseFolder, tempDirectory, jsonTarget);

        //filter la liste des jeux de donnée sur la maille et l'echelle
        let filteredList = dataSetService.filterOnMailleAndEchelle(dataSetList, maille, echelle)

        res.status(200).json(filteredList);

    } else {
        res.status(400).json({ message: "manque de query paramters echelle et maille" });
    }
}

/**
 * récuperation du fichier zip du jeux de donnée
 * @param {*} req 
 * @param {*} res 
 * @param {*} next 
 */
exports.getDataSetZip = (req, res, next) => {
    let dataSetName = req.params.dataSetName;

    let url = constants.dataSetBaseFolder + dataSetName  + '.zip';
    
    res.download(url);
}
