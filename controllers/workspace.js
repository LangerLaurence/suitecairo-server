const workspace = require('../services/readArchive');

exports.getWorkspacesList = (req, res, next) => {
    const tempDirectory = "./data/tempFiles/";
    const mapBaseFolder = "./data/Workspaces/";
    const jsonTarget = "workspace.json";

    let workspacesList = workspace.readZipArchive(mapBaseFolder, tempDirectory, jsonTarget);

    res.status(200).json(workspacesList);

}

exports.getWorkspacesModelsList = (req, res, next) => {
    const tempDirectory = "./data/tempFiles/";
    const mapBaseFolder = "./data/wsmodels/";
    const jsonTarget = "workspace.json";

    let workspacesModelsList = workspace.readZipArchive(mapBaseFolder, tempDirectory, jsonTarget);

    res.status(200).json(workspacesModelsList);

}

exports.exportWorkspaceFromClient = (req, res, next) => {
    let id = req.params.id;
    let destination = req.query.url;
};

exports.getWorkspace = (req, res, next) => {
    const workSpaceBaseFolder = "./data/Workspaces/";

    let id = req.params.id;

    let workspaceUrl = workspace.getZipPath(workSpaceBaseFolder, id);
    
    res.download(workspaceUrl);
}

exports.getWorkspaceModel = (req, res, next) => {
    const workSpaceBaseFolder = "./data/wsmodels/";

    let id = req.params.id;

    let workspaceUrl = workspace.getZipPath(workSpaceBaseFolder, id);
    
    res.download(workspaceUrl);
}

exports.uploadWorkspaceZip = (req, res, next) => {

    if(req.fileValidationError){
        res.status(400).json({ error: req.fileValidationError })
    }else{
        res.status(200).end();
    }

}