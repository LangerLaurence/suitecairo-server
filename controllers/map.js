
const map = require('../services/readArchive');

exports.getMapList = (req, res, next) => {
    const tempDirectory = "./data/tempFiles/";
    const mapBaseFolder = "./data/Fonds_de_cartes/";
    const jsonTarget = "mapInfo.json";

    let mapsList = map.readZipArchive(mapBaseFolder, tempDirectory, jsonTarget);

    res.status(200).json(mapsList);

}

exports.getMap = (req, res, next) => {
    const mapBaseFolder = "./data/Fonds_de_cartes/";

    let id = req.params.id;

    let mapUrl = map.getZipPath(mapBaseFolder, id);
    
    res.download(mapUrl);
}