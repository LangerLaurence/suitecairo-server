module.exports = Object.freeze({
    tempDirectory: './data/tempFiles/',
    dataSetBaseFolder: './data/data_sets/',
    contourBaseFolder: './data/contours/',
    dataSetJsonTargetFileName: 'data-set-info.json',
    contourJsonTargetFileName: 'data.json',
});
