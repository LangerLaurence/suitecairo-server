/**
 * filter une list de contours sur la maille et l'echelle
 * @param {*} list la contours à filterer 
 * @param {*} maille 
 * @param {*} echelle 
 * @returns 
 */
 function filterOnMailleAndEchelle(list, maille, echelle ){
    let filteredList = [];

    list.map((dataSet => {
        if(dataSet.echelle === echelle && dataSet.maille === maille){
            filteredList.push(dataSet);
        }
    }))
    return filteredList;
}

module.exports = {filterOnMailleAndEchelle}