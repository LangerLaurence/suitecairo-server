const express = require('express');
const cors = require("cors");

//definition des routes
const swaggerRoutes = require('./routes/swagger');
const mapRoutes = require('./routes/map');
const workspacesRoutes = require('./routes/workspace');
const dataSetRoutes = require('./routes/data-set');
const contourRoutes = require('./routes/contours.js');

const app = express();

app.use(cors());

app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content, Accept, Content-Type, Authorization');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
    next();
  });

app.use('/api-docs', swaggerRoutes);
app.use('/api/maps', mapRoutes);
app.use('/api/workspaces', workspacesRoutes);
app.use('/api/data-sets', dataSetRoutes);
app.use('/api/contours', contourRoutes);

module.exports = app;