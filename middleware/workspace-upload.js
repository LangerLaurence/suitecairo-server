const { path } = require('express/lib/application');
const multer = require('multer');

//Définir le middleware multer qui stock le fichier envoyer sur le serveur
const storage = multer.diskStorage({
    //Définir la destination du fichier
    destination: function(req, file, cb) {
        cb(null, './data/Workspaces/');
     },
     //Définir le nom du fichier. (dans notre cas, il garde le nom et extention d'origine)
    filename: function (req, file, cb) {
        cb(null , file.originalname);
    }
});
 
//Création d'un qui accepte seulement un fichier avec comme extention zip
const zipOnlyFilter = (req, file, cb) => { 

  if(file.mimetype == 'application/zip' ){
        cb(null,true);
  }else{
    req.fileValidationError = "seul un fichier ZIP est accepté";
    cb(null, false, req.fileValidationError);
  }
}

module.exports = multer({ storage: storage, fileFilter : zipOnlyFilter }).single('workspace');