const express = require('express');

const router = express.Router();
const  workspaceCtrl = require('../controllers/workspace')

const workspaceUpload = require('../middleware/workspace-upload.js')


router.get('/', workspaceCtrl.getWorkspacesList);
router.get('/:id', workspaceCtrl.getWorkspace);
router.post('/', workspaceUpload, workspaceCtrl.uploadWorkspaceZip);
router.get('/models', workspaceCtrl.getWorkspacesModelsList);
router.get('/models/:id', workspaceCtrl.getWorkspaceModel);

module.exports = router;