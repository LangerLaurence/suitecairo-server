const express = require('express');

const router = express.Router();
const  mapCtrl = require('../controllers/map')

router.get('/', mapCtrl.getMapList);
router.get('/:id', mapCtrl.getMap);

module.exports = router;