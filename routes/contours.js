const express = require('express');

const router = express.Router();
const  contourCtrl = require('../controllers/contours.js')

router.get('/', contourCtrl.getContourList);
router.get('/:contourName', contourCtrl.getContourZip);

module.exports = router;